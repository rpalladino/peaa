<?php

$peaa_db = new PDO("sqlite:" . __DIR__ . "/../db/peaa.db");

// DomainLogic
$peaa_db->query("CREATE TABLE products (ID int primary key, name text, type text)");
$peaa_db->query("CREATE TABLE contracts (ID int primary key, product int, revenue real, dateSigned int)");
$peaa_db->query("CREATE TABLE revenueRecognitions (contract int, amount real, recognizedOn int, PRIMARY KEY(contract, recognizedOn))");

// DataSourceArchitectural
$peaa_db->query("CREATE TABLE album (id integer primary key, name text, artistID integer key);");
$peaa_db->query("CREATE TABLE artist (id integer primary key, name text);");
$peaa_db->query("CREATE TABLE people (id integer primary key, lastname text, firstname text, number_of_dependents integer);");
$peaa_db->query("CREATE TABLE track (id integer primary key, albumID integer key, seq integer, title text);");

