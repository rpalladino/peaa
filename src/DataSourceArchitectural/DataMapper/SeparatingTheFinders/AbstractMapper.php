<?php

namespace SeparatingTheFinder;

use PDO;

abstract class AbstractMapper
{
	protected $db;
	protected $loadedMap;

	public function __construct(PDO $db)
	{
		$this->db = $db;
		$this->loadedMap = [];
	}

	abstract protected function findStatement();

	protected function abstractFind($id)
	{
		if (isset($this->loadedMap[$id])) {
			return $this->loadedMap[$id];	
		} 
		try {
			$findStatement = $this->db->prepare($this->findStatement());
			$findStatement->bindValue(1, $id, PDO::PARAM_INT);
			$findStatement->execute();
			$rs = $findStatement->fetch();
			$result = $this->load($rs);
			return $result;
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	protected function load (array $rs) {
		$id = $rs["id"];
		if (isset($this->loadedMap[$id])) {
			return $this->loadedMap[$id];
		}
		$result = $this->doLoad($id, $rs);
		$this->loadedMap[] = $result;
		return $result;
	}

	abstract protected function doLoad($id, array $rs);
}
