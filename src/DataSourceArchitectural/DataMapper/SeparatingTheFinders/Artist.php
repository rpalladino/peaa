<?php

namespace SeparatingTheFinder;

class Artist
{
	protected $id;
	protected $name;

	public function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name;
	}

	public function __get($name)
	{
		return $this->$name;
	}
}
