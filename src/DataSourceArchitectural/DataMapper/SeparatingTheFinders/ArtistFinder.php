<?php

namespace SeparatingTheFinder;

interface ArtistFinder
{
	function find($id);
}