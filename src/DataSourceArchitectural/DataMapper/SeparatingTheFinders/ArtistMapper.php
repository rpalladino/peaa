<?php

namespace SeparatingTheFinder;

class ArtistMapper extends AbstractMapper implements ArtistFinder
{
	const COLUMN_LIST = "art.ID, art.name";

	public function find($id)
	{
		return $this->abstractFind($id);
	}

	protected function findStatement()
	{
		return "select " . self::COLUMN_LIST . " from artist art where ID = ?";
	}

	protected function doLoad($id, array $rs)
	{
		$name = $rs["name"];
		$result = new Artist($id, $name);
		return $result;
	}
}