<?php

namespace SeparatingTheFinder;

class Track
{
	private $id;
	private $albumID;
	private $sequence;
	private $title;

	public function __construct($id, $albumID, $seq, $title)
	{
		$this->id = $id;
		$this->albumID = $albumID;
		$this->sequence = $seq;
		$this->title = $title;
	}

	public function __get($name)
	{
		return $this->$name;
	}
}