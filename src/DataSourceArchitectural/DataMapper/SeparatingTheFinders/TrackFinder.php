<?php

namespace SeparatingTheFinder;

interface TrackFinder
{
	public function find($id);
	public function findForAlbum($albumId);
}
