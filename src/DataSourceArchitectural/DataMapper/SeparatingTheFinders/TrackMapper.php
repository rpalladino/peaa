<?php

namespace SeparatingTheFinder;

use PDO;

class TrackMapper extends AbstractMapper implements TrackFinder
{
	public function find($id)
	{
		return null;
	}

	public function findForAlbum($albumID)
	{
		try {
			$stmt = $this->db->prepare($this->findForAlbumStatement());
			$stmt->bindValue(1, $albumID, PDO::PARAM_INT);
			$stmt->execute();
			$result = [];
			while ($row = $stmt->fetch()) {
				$result[] = $this->load($row);
			}
			return $result;
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	protected function doLoad($id, array $row)
	{
		$albumID = $row["albumID"];
		$seq = $row["seq"];
		$title = $row["title"];
		$result = new Track($id, $albumID, $seq, $title);
		return $result;
	}

	protected function findStatement()
	{
		return "";
	}

	protected function findForAlbumStatement()
	{
		return "SELECT ID, seq, albumID, title
				FROM track
				WHERE albumID = ? ORDER BY seq";
	}
}
