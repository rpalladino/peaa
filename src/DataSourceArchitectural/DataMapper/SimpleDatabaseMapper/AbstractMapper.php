<?php

abstract class AbstractMapper
{
	protected $db;
	protected $loadedMap;

	public function __construct(PDO $db)
	{
		$this->db = $db;
		$this->loadedMap = [];
	}

	public function findMany(StatementSource $source)
	{
		try {
			$stmt = $this->db->prepare($source->sql());
			for($i = 0; $i < $source->parameters()->count(); $i++)
			{
				$stmt->bindValue($i+1, $source->parameters()[$i]);
			}
			$stmt->execute();
			return $this->loadAll($stmt);	
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	public function insert(Person $subject)
	{
		try {
			$insertStatement = $this->db->prepare(static::insertStatement());
			$this->doInsert($subject, $insertStatement);
			$this->loadedMap[$subject->id] = $subject;
			return $subject->id; 
		} catch (PDOStatement $e) {
			throw new ApplicationException($e);
		}		
	}

	abstract static protected function insertStatement();

	abstract protected function doInsert($subject, PDOStatement $insertStatement);

	abstract static protected function findStatement();

	protected function abstractFind($id)
	{
		if (isset($this->loadedMap[$id])) {
			return $this->loadedMap[$id];	
		} 
		try {
			$findStatement = $this->db->prepare($this->findStatement());
			$findStatement->bindValue(1, $id, PDO::PARAM_INT);
			$findStatement->execute();
			$rs = $findStatement->fetch();
			$result = $this->load($rs);
			return $result;
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	protected function load (array $rs) {
		$id = $rs["id"];
		if (isset($this->loadedMap[$id])) {
			return $this->loadedMap[$id];
		}
		$result = $this->doLoad($id, $rs);
		$this->loadedMap[] = $result;
		return $result;
	}

	abstract protected function doLoad($id, array $rs);

	protected function loadAll(PDOStatement $rs) {
		$result = new ArrayObject();
		foreach($rs as $row)
		{
			$result->append($this->load($row));
		}
		return $result;
	}
}

// @todo Move this
class ApplicationException extends Exception {}