<?php 

class Person
{
	protected $id;
	private $lastName;
	private $firstName;
	private $numberOfDependents;

	protected static function settableProperties()
	{
		return ['id', 'lastName', 'firstName', 'numberOfDependents'];
	}

	public function __construct($id, $lastName, $firstName, $numberOfDependents)
	{
		$this->id = $id;
		$this->lastName = $lastName;
		$this->firstName = $firstName;
		$this->numberOfDependents = $numberOfDependents;
	}

	public function __get($name)
	{
		return $this->$name;
	}

	public function __set($name, $value)
	{
		if (in_array($name, static::settableProperties())) {
			$this->$name = $value;
		}
	}
}