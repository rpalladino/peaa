<?php

use PersonMapper\FindByLastName;

class PersonMapper extends AbstractMapper
{
	const COLUMNS = "id, lastname, firstname, number_of_dependents";

	protected static function findLastNameStatement() {
		return sprintf(
			"SELECT %s FROM people 
			 WHERE UPPER(lastname) like UPPER(?)
			 ORDER BY lastname", 
			self::COLUMNS
		);
	}

	protected static function findStatement() {
		return sprintf(
			"SELECT %s FROM people WHERE id = ?",
			self::COLUMNS
		);
	}

	protected static function updateStatement()
	{
		return "UPDATE people
				SET lastname = ?, firstname = ?, number_of_dependents = ?
				WHERE id = ?";
	}

	protected static function insertStatement()
	{
		return "INSERT INTO people VALUES (?, ?, ?, ?)";
	}

	protected function doInsert($subject, PDOStatement $insertStatement)
	{
		if (! $subject instanceof Person)
		{
			throw new InvalidArgumentException();
		}
		$insertStatement->bindValue(1, null);
		$insertStatement->bindValue(2, $subject->lastName, PDO::PARAM_STR);
		$insertStatement->bindValue(3, $subject->firstName, PDO::PARAM_STR);
		$insertStatement->bindValue(4, $subject->numberOfDependents, PDO::PARAM_INT);
		$insertStatement->execute();
		return $this->getInsertedId($subject);
	}

	protected function getInsertedId($subject)
	{
		$result = $this->db->query("SELECT last_insert_rowid() as id");
		$subject->id = (int) $result->fetch()["id"];
		return $subject->id;
	}

	public function find($id)
	{
		if (! is_int($id)) {
			throw new InvalidArgumentException();
		}
		return $this->abstractFind($id);
	}

	public function doLoad($id, array $rs)
	{
		$lastNameArg = $rs[1];
		$firstNameArg = $rs[2];
		$numDependentsArg = (int) $rs[3];
		return new Person($id, $lastNameArg, $firstNameArg, $numDependentsArg);
	}

	public function findByLastName($name)
	{
		try {
			$stmt = $this->db->prepare(static::findLastNameStatement());
			$stmt->bindParam(1, $name, PDO::PARAM_STR);
			$stmt->execute();
			return $this->loadAll($stmt);
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	public function findByLastName2($name)
	{
		return $this->findMany(new FindByLastName($name));
	}

	public function update(Person $subject)
	{
		try {
			$updateStatement = $this->db->prepare(static::updateStatement());
			$updateStatement->bindValue(1, $subject->lastName, PDO::PARAM_STR);
			$updateStatement->bindValue(2, $subject->firstName, PDO::PARAM_STR);
			$updateStatement->bindValue(3, $subject->numberOfDependents, PDO::PARAM_INT);
			$updateStatement->bindValue(4, $subject->id);
			$updateStatement->execute();
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}

	public function clean()
	{
		$this->loadedMap = [];
	}
}
