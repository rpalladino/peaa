<?php

namespace PersonMapper;

class FindByLastName implements \StatementSource
{
	private $lastName;

	public function __construct($name)
	{
		$this->lastName = $name;
	}

	public function sql()
	{
		return sprintf(
			"SELECT %s FROM people 
			 WHERE UPPER(lastname) like UPPER(?)
			 ORDER BY lastname", 
			\PersonMapper::COLUMNS
		);
	}

	public function parameters()
	{
		return new \ArrayObject([$this->lastName]);
	}
}