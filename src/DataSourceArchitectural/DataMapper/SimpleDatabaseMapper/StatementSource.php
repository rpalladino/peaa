<?php

interface StatementSource
{
	public function sql();
	public function parameters();
}
