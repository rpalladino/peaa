<?php

namespace DomainModel;

class CompleteRecognitionStrategy implements RecognitionStrategy
{
	public function calculateRevenueRecognitions(Contract $contract)
	{
		$contract->addRevenueRecognition(new RevenueRecognition(
			$contract->getRevenue(),
			$contract->getWhenSigned()
		));
	}
}