<?php 

namespace DomainModel;

use DateTime;
use Money;

class Contract
{
	private $product;
	private $revenue;
	private $whenSigned;
	private $id;
	private $revenueRecognitions = [];

	public function __construct(Product $product, Money $revenue, DateTime $whenSigned)
	{
		$this->product = $product;
		$this->revenue = $revenue;
		$this->whenSigned = $whenSigned;
	}

	public function getRevenue()
	{
		return $this->revenue;
	}

	public function getWhenSigned()
	{
		return $this->whenSigned;
	}

	public function getRecognitions()
	{
		return $this->revenueRecognitions;
	}

	public function recognizedRevenue(DateTime $asof)
	{
		$result = Money::dollars(0);
		foreach ($this->revenueRecognitions as $revenue)
		{
			if ($revenue->isRecognizableBy($asOf))
			{
				$result->add($revenue->getAmount());	
			}
		}
		return $result;
	}

	public function calculateRecognitions()
	{
		$this->product->calculateRevenueRecognitions($this);
	}

	public function addRevenueRecognition(RevenueRecognition $recognition)
	{
		$this->revenueRecognitions[] = $recognition;
	}
}