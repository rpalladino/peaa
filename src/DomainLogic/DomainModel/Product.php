<?php

namespace DomainModel;

class Product
{
	private $name;
	private $recognitionStrategy;

	public function __construct($name, RecognitionStrategy $recognitionStrategy)
	{
		$this->name = $name;
		$this->recognitionStrategy = $recognitionStrategy;
	}

	public function calculateRevenueRecognitions(Contract $contract)
	{
		$this->recognitionStrategy->calculateRevenueRecognitions($contract);
	}

	public static function newWordProcessor($name)
	{
		return new Product($name, new CompleteRecognitionStrategy());
	}

	public static function newSpreadsheet($name)
	{
		return new Product($name, new ThreeWayRecognitionStrategy(60, 90));
	}

	public static function newDatabase($name)
	{
		return new Product($name, new ThreeWayRecognitionStrategy(30, 60));
	}
}