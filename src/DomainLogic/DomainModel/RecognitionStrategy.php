<?php

namespace DomainModel;

interface RecognitionStrategy
{
	public function calculateRevenueRecognitions(Contract $contract);
}