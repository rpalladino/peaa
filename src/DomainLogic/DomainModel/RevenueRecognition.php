<?php

namespace DomainModel;

use DateTime;
use Money;

class RevenueRecognition
{
	private $amount;
	private $date;

	public function __construct(Money $amount, DateTime $date)
	{
		$this->amount = $amount;
		$this->date = $date;
	}

	public function getAmount()
	{
		return $this->amount;
	}

	public function isRecognizableBy(DateTime $asOf)
	{
		return $asOf >= $this->date;
	}
}
