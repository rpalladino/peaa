<?php

namespace DomainModel;

use DateInterval;

class ThreeWayRecognitionStrategy implements RecognitionStrategy
{
	private $firstRecognitionOffset;
	private $secondRecognitionOffset;

	public function __construct($firstRecognitionOffset, $secondRecognitionOffset)
	{
		$this->firstRecognitionOffset = new DateInterval("P" . $firstRecognitionOffset . "D");
		$this->secondRecognitionOffset = new DateInterval("P" . $secondRecognitionOffset . "D");
	}

	public function calculateRevenueRecognitions(Contract $contract)
	{
		$allocation = $contract->getRevenue()->allocate(3);
		$contract->addRevenueRecognition(new RevenueRecognition(
			$allocation[0], $contract->getWhenSigned()
		));
		$contract->addRevenueRecognition(new RevenueRecognition(
			$allocation[1], $contract->getWhenSigned()->add($this->firstRecognitionOffset)
		));
		$contract->addRevenueRecognition(new RevenueRecognition(
			$allocation[2], $contract->getWhenSigned()->add($this->secondRecognitionOffset)
		));
	}
}