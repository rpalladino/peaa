<?php

interface EmailGateway
{
	public function sendEmailMessage($toAddress, $subject, $body);
}
