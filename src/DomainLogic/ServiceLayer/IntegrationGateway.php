<?php

use DomainModel\Contract;

interface IntegrationGateway
{
	public function publishRevenueRecognition(Contract $contract);
}
