<?php

use DomainModel\Contract;

class RecognitionService extends ApplicationService
{
	public function calculateRevenueRecognitions($contractNumber)
	{
		$contract = Contract::readForUpdate($contractNumber);
		$contract->calculateRecognitions();

		$this->getEmailGateway()->sendEmailMessage(
			$contract->getAdministratorEmailAddress(),
			"RE: Contract #" . $contractNumber,
			$contract . " has had revenue recognitions calculated"
		);
		$this->getIntegrationGateway()->publishRevenueRecognitionCalculation($contract);
	}

	public function recognizedRevenue($contractNumber, DateTime $asOf)
	{
		return Contract::read($contractNumber)->recognizedRevenue($asOf);
	}
}
