<?php 

namespace TransactionScript;

use DateTime;
use Money;
use PDO;

class Gateway
{
	private $db;

	static private $findRecognitionsStatement = 
		"SELECT amount 
		 FROM revenueRecognitions 
		 WHERE contract = ? and recognizedOn <= ?";

	static private $findContractStatement =
		"SELECT *
		 FROM contracts c, products p
		 WHERE c.ID = ? AND c.product = p.ID";

	static private $insertRecognitionStatement = 
		"INSERT INTO revenueRecognitions VALUES (?, ?, ?)";

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}

	public function findRecognitionsFor($contractId, \DateTime $asof)
	{
		$stmt = $this->db->prepare(self::$findRecognitionsStatement);
		$stmt->bindValue(1, $contractId, \PDO::PARAM_INT);
		$stmt->bindValue(2, $asof->format("U"), \PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function findContract($contractId)
	{
		$stmt = $this->db->prepare(self::$findContractStatement);
		$stmt->bindValue(1, $contractId, PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function insertRecognition($contractId, Money $amount, DateTime $asof)
	{
		$stmt = $this->db->prepare(self::$insertRecognitionStatement);
		$stmt->bindValue(1, $contractId, PDO::PARAM_INT);
		$stmt->bindValue(2, $amount->getAmount());
		$stmt->bindValue(3, (int) $asof->format("U"), PDO::PARAM_INT);
		$stmt->execute();
	}
}