<?php

class Money {
 
    private $amount;
    private $currency;
 
    public function __construct($amount, Currency $currency) {
        $this->amount = $amount;
        $this->currency = $currency;
    }
 
    static public function dollars($amount)
    {
        return new Money($amount, Currency::USD());
    }

    public function getAmount() {
        return $this->amount;
    }
     
    public function getCurrency() {
        return $this->currency;
    }
     
    public function add(Money $other) {
        $this->ensureSameCurrencyWith($other);
        return new Money($this->amount + $other->getAmount(), $this->currency);
    }

    public function subtract(Money $other) {
        $this->ensureSameCurrencyWith($other);
        if ($other > $this)
            throw new Exception("Subtracted money is more than what we have");
        return new Money($this->amount - $other->getAmount(), $this->currency);
    }
     
    public function multiplyBy($multiplier, $roundMethod = PHP_ROUND_HALF_UP) {
        $product = round($this->amount * $multiplier, 0, $roundMethod);
        return new Money($product, $this->currency);
    }
     
    private function ensureSameCurrencyWith(Money $other) {
        if ($this->currency != $other->getCurrency())
            throw new Exception("Both Moneys must be of same currency");
    }

    public function allocate($n)
    {
        $lowResult = new Money($this->amount / $n, $this->currency);
        $highResult = new Money($lowResult->amount + 1, $this->currency);
        $results = [];
        $remainder = (int) $this->amount % $n;
        for ($i = 0; $i < $remainder; $i++) {
            $results[$i] = $highResult;
        }
        for ($i = 0; $i < $n; $i++) {
            $results[$i] = $lowResult;
        }
        return $results;
    }

}
