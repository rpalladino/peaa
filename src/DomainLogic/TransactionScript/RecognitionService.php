<?php

namespace TransactionScript;

use DateTime;
use DateInterval;
use Money;

class RecognitionService
{
	private $db;

	public function __construct(Gateway $db)
	{
		$this->db = $db;
	}

	public function recognizedRevenue($contractNumber, \DateTime $asof)
	{
		$result = 0;
		try {
			$rs = $this->db->findRecognitionsFor($contractNumber, $asof);
			foreach ($rs as $row) {
				$result += $row["amount"];
			}
		} catch (\PDOException $e) {
			throw new ApplicationException($e);
		}
		return $result;
	}

	public function calculateRevenueRecognitions($contractNumber)
	{
		try {
			$contract = $this->db->findContract($contractNumber);
			$totalRevenue = Money::dollars($contract["revenue"]);
			$recognitionDate = DateTime::createFromFormat(
				"U", $contract["dateSigned"]
			);
			$type = $contract["type"];
			if ($type == "S") {
				$allocation = $totalRevenue->allocate(3);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate
				);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate->add(new DateInterval("P60D"))
				);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate->add(new DateInterval("P90D"))
				);
			} elseif ($type == "W") {
				$this->db->insertRecognition(
					$contractNumber, $totalRevenue, $recognitionDate
				);
			} elseif ($type == "D") {
				$allocation = $totalRevenue->allocate(3);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate
				);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate->add("P30D")
				);
				$this->db->insertRecognition(
					$contractNumber, $allocation[0], $recognitionDate->add("P60D")
				);
			}
		} catch (PDOException $e) {
			throw new ApplicationException($e);
		}
	}
}