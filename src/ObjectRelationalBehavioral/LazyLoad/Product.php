<?php

class Product
{
	private $id;
	private $name;
	
	static private $collection = [];

	public function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name;
	}

	public function __get($name)
	{
		return $this->$name;
	}

	public function insert()
	{
		self::$collection[] = $this;
	}

	static public function findForSupplier()
	{
		return self::$collection;
	}

	static public function clean()
	{
		self::$collection = [];
	}
}