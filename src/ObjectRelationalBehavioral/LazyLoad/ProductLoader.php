<?php

class ProductLoader implements ValueLoader
{
	private $id; 

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function load()
	{
		return Product::findForSupplier($this->id);
	}
}