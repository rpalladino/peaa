<?php

class Supplier 
{
	private $id;
	private $products;

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getProducts()
	{
		if (is_null($this->products)) {
			$this->products = Product::findForSupplier($this->id);
		}
		return $this->products;
	}
}