<?php

class SupplierVH
{
	private $id;
	private $products;

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getProducts()
	{
		return $this->products->getValue();
	}

	public function setProducts(ValueHolder $value)
	{
		$this->products = $value;
	}
}