<?php

class ValueHolder
{
	private $value;
	private $loader;

	public function __construct(ValueLoader $loader)
	{
		$this->loader = $loader;
	}

	public function getValue()
	{
		if (is_null($this->value)) $this->value = $this->loader->load();
		return $this->value;
	}
}
