<?php

class ColumnMap
{
	private $columnName;
	private $fieldName;
	private $field;
	private $dataMap;

	public function __construct($columnName, $fieldName, $dataMap)
	{
		$this->columnName = $columnName;
		$this->fieldName = $fieldName;
		$this->dataMap = $dataMap;
		$this->initField();
	}

	private function initField()
	{
		try {
			$this->field = $this->dataMap->getDomainClass()->getDeclaredField($this->fieldName);
			$this->field->setAccessible(true);
		} catch (Exception $e) {
			throw new ApplicationException("unable to set up field: " $this->fieldName, $e);
		}
	}
}