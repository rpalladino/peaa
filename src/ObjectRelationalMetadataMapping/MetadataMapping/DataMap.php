<?php

class DataMap
{
	private $domainClass;
	private $tableName;
	private $columnMaps = [];

	public function columnList()
	{
		$result = " ID"; //use map/reduce here?
		foreach ($this->columnMaps as $columnMap) {
			// no guarantee that $columnMap is a ColumnMap, but this is a dynamic language, after all
			$result .= ", " . $columnMap->getColumnName();
		}
		return $result;
	}

	public function getTableName()
	{
		return $tableName;
	}
}