<?php

class Mapper
{
	private $uow;
	private $dataMap;
	private $db;

	public function findObject($key)
	{
		$this->returnObjectIfLoaded($key);
		$sql = "SELECT " . $this->dataMap->columnList() . " FROM " . $this->dataMap->getTableName() . " WHERE ID = ?";
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(1, $key);
			$stmt->execute();
			$result = $this->load($stmt);
		} catch (Exception $e)
		{
			throw new ApplicationException($e);
		}
		return $result;
	}

	public function load(PDOStatement $stmt)
	{
		$rs = $stmt->fetch();
		$key = $rs["ID"];
		$this->returnObjectIfLoaded($key);
		$class = $this->dataMap->getDomainClass();
		$result = new $class;
		
	}

	private function returnObjectIfLoaded($key)
	{
		if ($this->uow->isLoaded($key)) return $this->uow->getObject($key);
	}
}