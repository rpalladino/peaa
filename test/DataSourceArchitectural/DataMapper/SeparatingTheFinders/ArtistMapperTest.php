<?php

namespace SeparatingTheFinder;

use PDO;
use PHPUnit_Framework_TestCase;

class ArtistMapperTest extends PHPUnit_Framework_TestCase
{
	private $db;
	private $mapper;

	function setUp()
	{
		$path = __DIR__ . "/../../../../db/peaa.db";
		$this->db = new PDO("sqlite:" . $path);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->mapper = new ArtistMapper($this->db);
	}

	function testItCanFindAnArtistById()
	{
		$this->insertAnArtist();
		$artist = $this->mapper->find(1);
		$this->assertEquals("Jane Monheit", $artist->name);
	}

	protected function insertAnArtist()
	{
		$sql = "insert into artist values (null, 'Jane Monheit')";
		$this->db->query($sql);
	}

	protected function resetDatabase()
	{
		$this->db->query("delete from artist");
	}

	function tearDown()
	{
		$this->resetDatabase();
	}
}