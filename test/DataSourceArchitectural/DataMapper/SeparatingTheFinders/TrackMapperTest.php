<?php

namespace SeparatingTheFinder;

use PDO;
use PHPUnit_Framework_TestCase;

class TrackMapperTest extends PHPUnit_Framework_TestCase
{
	private $db;
	private $mapper;

	function setUp()
	{
		$path = __DIR__ . "/../../../../db/peaa.db";
		$this->db = new PDO("sqlite:" . $path);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->mapper = new TrackMapper($this->db);
	}

	function testItCanFindTracksByAlbumId()
	{
		$this->insertAlbumWithTracks();
		$tracks = $this->mapper->findForAlbum(1);
		$this->assertEquals(1, count($tracks));
		$this->assertEquals("Honeysuckle Rose", $tracks[0]->title);
	}

	protected function insertAlbumWithTracks()
	{
		$sql = "INSERT INTO album (id, name, artistID) 
				VALUES (1, 'Taking a Chance on Love', 1)";
		$this->db->query($sql);

		$sql = "INSERT INTO track (id, albumID, seq, title) 
				VALUES (1, 1, 1, 'Honeysuckle Rose')";
		$this->db->query($sql);
	}

	protected function resetDatabase()
	{
		$this->db->query("delete from album");
		$this->db->query("delete from track");
	}

	function tearDown()
	{
		$this->resetDatabase();
	}
}