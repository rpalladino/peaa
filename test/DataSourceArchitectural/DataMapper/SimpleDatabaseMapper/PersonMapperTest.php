<?php

class PersonMapperTest extends PHPUnit_Framework_TestCase
{
	private $db;
	private $mapper;

	function setUp()
	{
		$path = __DIR__ . "/../../../../db/peaa.db";
		$this->db = new PDO("sqlite:" . $path);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->mapper = new PersonMapper($this->db);
	}

	function testItCanBeInstantiated()
	{	
		$this->assertEquals("PersonMapper", get_class($this->mapper));
	}

	function testItCanFindAPerson()
	{
		$this->insertAPerson(1, 'Palladino', 'Rocco', 3);

		$person = $this->mapper->find(1);
		$this->assertEquals("Rocco", $person->firstName);
		$this->assertEquals("Palladino", $person->lastName);
		$this->assertEquals("3", $person->numberOfDependents);
	}

	function testItCanFindManyPeopleByLastName()
	{
		$this->insertPalladinoFamily();
		$people = $this->mapper->findByLastName('Palladino');
		$this->assertEquals(4, count($people));
		foreach($people as $person) {
			$this->assertEquals('Palladino', $person->lastName);
		}
	}

	function testItCanFindManyPeopleByLastName2()
	{
		$this->insertPalladinoFamily();
		$people = $this->mapper->findByLastName2('Palladino');
		$this->assertEquals(4, count($people));
		foreach($people as $person) {
			$this->assertEquals('Palladino', $person->lastName);
		}
	}

	function testItCanUpdateAPerson()
	{
		$this->insertAPerson(1, 'Palladino', 'Rocky', 3);

		$person = $this->mapper->find(1);
		$person->firstName = "Rocco";
		$this->mapper->update($person);
		$this->mapper->clean();

		$person = $this->mapper->find(1);
		$this->assertEquals("Rocco", $person->firstName);
	}

	function testItCanInsertAPerson()
	{
		$bob = new Person(null, "Martin", "Uncle Bob", 1);

		$this->mapper->insert($bob);
		$id = $bob->id;
		//var_dump($id, $bob);exit;
		$this->mapper->clean();
		$bob = $this->mapper->find($id);
		$this->assertEquals("Martin", $bob->lastName);
		$this->assertEquals("Uncle Bob", $bob->firstName);
	}

	protected function insertAPerson($id, $lastName, $firstName, $numberOfDependents)
	{
		$sql = sprintf(
			"insert into people values (%s, '%s', '%s', %s)",
			$id, $lastName, $firstName, $numberOfDependents
		);
		$this->db->query($sql);
	}

	protected function insertPalladinoFamily()
	{
		$this->insertAPerson(1, 'Palladino', 'Rocco', 2);
		$this->insertAPerson(2, 'Palladino', 'Hallie', 2);
		$this->insertAPerson(3, 'Palladino', 'Rocky', 0);
		$this->insertAPerson(4, 'Palladino', 'Serena', 0);
	}

	protected function resetDatabase()
	{
		$this->db->query("delete from people");
	}

	function tearDown()
	{
		$this->resetDatabase();
	}
}