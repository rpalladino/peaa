<?php

namespace DomainModel;

use DateTime;
use Money;

class RevenueRecognitionTest extends \PHPUnit_Framework_TestCase
{
	private $word;
	private $calc;
	private $db;


	function setUp()
	{
		$this->word = Product::newWordProcessor("Thinking Word");
		$this->calc = Product::newSpreadsheet("Thinking Calc");
		$this->db = Product::newDatabase("Thinking DB");
	}

	function testAContractCanCalculateRecognitions()
	{
		$wordContract = new Contract(
			$this->word, 
			Money::dollars(1000), 
			DateTime::createFromFormat("Y-m-d", "2014-08-02")
		);
		
		$this->assertEquals(0, count($wordContract->getRecognitions()));
		
		$wordContract->calculateRecognitions();
		$recognitions = $wordContract->getRecognitions();
		$this->assertEquals(1, count($recognitions));
		$this->assertEquals(Money::dollars(1000), $recognitions[0]->getAmount());
	}
}