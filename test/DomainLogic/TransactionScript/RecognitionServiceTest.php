<?php

namespace TransactionScript;

use PDO;
use DateTime;
use DateInterval;

class RecognitionServiceTest extends \PHPUnit_Framework_TestCase
{
	private $db;
	private $today;
	private $recognitionService;

	function setUp()
	{
		$this->db = new PDO("sqlite:" . __DIR__ . "/../../../db/peaa.db");
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$this->recognitionService = new RecognitionService(
			new Gateway($this->db)
		);

		$this->createProducts();
		$this->createContracts();
	}

	function tearDown()
	{
		$this->resetDatabase();
	}

	function testItCanGetRecognizedRevenueForContract()
	{
		$contractId = 1;
		$asof = $this->today();

		$result = $this->recognitionService->recognizedRevenue($contractId, $asof);
	}

	function testItCanCalculateRecognizedRevenueForWordProcessorContracts()
	{
		$wordContractId = 1;
		$this->recognitionService->calculateRevenueRecognitions($wordContractId);

		$result = $this->db->query("SELECT * FROM revenueRecognitions WHERE contract = 1");
		$revenueRecognitions = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$this->assertEquals(1, count($revenueRecognitions));
		$this->assertEquals(1000.00, (int) $revenueRecognitions[0]["amount"]);
		$this->assertEquals(
			$this->today()->format("U"), 
			$revenueRecognitions[0]["recognizedOn"]
		);
	}

	function testItCanCalculateRecognizedRevenueForSpreadsheetContracts()
	{
		$spreadsheetId = 2;
		$this->recognitionService->calculateRevenueRecognitions($spreadsheetId);

		$result = $this->db->query("SELECT * FROM revenueRecognitions WHERE contract = 2");
		$revenueRecognitions = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$this->assertEquals(3, count($revenueRecognitions));
		$this->assertEquals(666.67, round($revenueRecognitions[0]["amount"], 2));
		$this->assertEquals(666.67, round($revenueRecognitions[1]["amount"], 2));
		$this->assertEquals(666.67, round($revenueRecognitions[2]["amount"], 2));

		$date1 = clone $this->today();
		$date2 = clone $this->today()->add(new DateInterval("P60D"));
		$date3 = clone $this->today()->add(new DateInterval("P90D"));
		
		$actualDate1 = new DateTime;
		$actualDate1->setTimestamp($revenueRecognitions[0]["recognizedOn"]);
		$this->assertEquals($date1->format("U"), $actualDate1->format("U"));
		
		$actualDate2 = new DateTime;
		$actualDate2->setTimestamp($revenueRecognitions[1]["recognizedOn"]);
		$this->assertEquals($date2->format("U"), $actualDate2->format("U"));
		
		//$actualDate3 = new DateTime;
		//$actualDate3->setTimestamp($revenueRecognitions[2]["recognizedOn"]);
		//$this->assertEquals($date3->format("U"), $actualDate3->format("U"));
	}

	private function createProducts()
	{
		$this->db->query("INSERT INTO products VALUES (1, 'Word', 'W')");
		$this->db->query("INSERT INTO products VALUES (2, 'Excel', 'S')");
		$this->db->query("INSERT INTO products VALUES (3, 'Access', 'D')");
	}

	private function createContracts()
	{
		$this->db->query("INSERT INTO contracts VALUES (1, 1, 1000.00, " . $this->today()->format("U") . ")");
		$this->db->query("INSERT INTO contracts VALUES (2, 2, 2000.00, " . $this->today()->format("U") . ")");
		$this->db->query("INSERT INTO contracts VALUES (3, 3, 3000.00, " . $this->today()->format("U") . ")");
	}

	private function resetDatabase()
	{
		$this->db->query("DELETE FROM products");
		$this->db->query("DELETE FROM contracts");
		$this->db->query("DELETE FROM revenueRecognitions");
	}

	private function today()
	{
		if (is_null($this->today)) $this->today = new DateTime();
		return $this->today;
	}

}