<?php

abstract class LazyLoadTestCase extends PHPUnit_Framework_TestCase
{
	function testSupplierCanLazyLoadProducts()
	{
		$supplier = $this->createSupplier();
		$this->addProductsForSupplier($supplier->getId());
		$this->assertProductsInitiallyNull($supplier);
		$this->assertProductsLazyLoaded($supplier);
	}	

	function tearDown()
	{
		Product::clean();
	}

	abstract protected function createSupplier($supplierId = 1);

	protected function addProductsForSupplier($supplierId)
	{
		$products = [];
		$products[] = new Product($supplierId, 'Book');
		$products[] = new Product($supplierId, 'Game');
		$products[] = new Product($supplierId, 'Chair');
		foreach($products as $product) {
			$product->insert();
		}
	}

	protected function assertProductsInitiallyNull($supplier)
	{
		$this->assertAttributeEquals(null, 'products', $supplier);
	}

	private function assertProductsLazyLoaded($supplier)
	{
		$loadedProducts = $supplier->getProducts();
		$this->assertEquals(3, count($loadedProducts));
		$this->assertEquals("Book", $loadedProducts[0]->name);
		$this->assertEquals("Game", $loadedProducts[1]->name);
		$this->assertEquals("Chair", $loadedProducts[2]->name);
	}
}
