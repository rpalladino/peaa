<?php

require_once 'LazyLoadTestCase.php';

class ValueHolderTest extends LazyLoadTestCase
{
	protected function createSupplier($supplierId = 1)
	{
		$supplier = new SupplierVH($supplierId);
		$supplier->setProducts(new ValueHolder(new ProductLoader($supplierId)));
		return $supplier;
	}

	protected function assertProductsInitiallyNull($supplier)
	{
		// use reflection to get the initial value holder value
		
		// make the Supplier#products property accessible
		$supplierClass = new ReflectionClass('SupplierVH');
		$productsProperty = $supplierClass->getProperty('products');
		$productsProperty->setAccessible(true);

		// make the ValueHolder#value property accessible
		$valueHolderClass = new ReflectionClass('ValueHolder');
		$valueProperty = $valueHolderClass->getProperty('value');
		$valueProperty->setAccessible(true);		

		// get the products (value holder) from the supplier
		$valueHolder = $productsProperty->getValue($supplier);

		// get the value from the value holder
		$value = $valueProperty->getValue($valueHolder);

		$this->assertEquals(null, $value);
	}
}
